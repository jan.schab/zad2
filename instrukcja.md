##Zadanie 1 – 2pkt
Po zapoznaniu się z podstawami składni:
[https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
proszę stworzyć dokument tekstowy zad1.md zawierający następujące elementy:
1. wybrany plik graficzny,
2. listę numerowaną trzech ulubionych książek,
3. listę nienumerowaną pięciu miast w Polsce,
4. dwa odnośniki do wybranych zasobów internetowych,
5. pojedyncze zdanie zawierające wyrazy wyróżnione w trybach: bold, italic, striketrough,
6. wybrany wzór matematyczny w formacie Latex,
7. dwie linie teksu z **nagłówkami** poziomu 2 i 3.

Poprawność dokumentu proszę sprawdzić wykorzystując dedykowany edytor np. **Typora**. Jako
wynik przesłać plik zad1.md
